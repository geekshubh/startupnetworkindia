<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
  public function pvt_ltd()
  {
    return view('pvt-ltd');
  }

  public function proprietorship()
  {
    return view('proprietorship');
  }

  public function llp()
  {
    return view('llp');
  }

  public function opc()
  {
    return view('opc');
  }

  public function partnership()
  {
    return view('partnership');
  }
}
