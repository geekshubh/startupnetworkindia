@extends('layouts.app')
@section('content')
<!-- ***** About Us Area Start *****  -->
<section class="about_us_area section_padding_90_100 clearfix" id="about">
  <div class="container">
    <div class="row">
      <!-- Heading Text -->
      <div class="col-12">
        <div class="section-heading text-center">
          <h2>Private Limited Company</h2>
        </div>
      </div>
    </div>
    <div class="row align-items-center">
      <div class="col-12 col-md-12">
        <!-- About us Content -->
        <div class="about_us_content">
          <h2>What is Private Limited Company?</h2>
          Private Limited Company is most suited for fast growing companies wanting to raise funds. It is the most credible structure of doing a business in India. Private Limited company can be registered in 10 working days. Once registered the government issues a certificate of incorporation which is the basic document. After registration you just need to open a bank account, deposit the share capital and commence your business. The private limited company can be registered any where in India and it can do business anywhere irrespective of its registered address.
          <br>
          <br>
          The basic requirements are as follows:
          <ol type = 1>
            <li>1. Minimum 2 directors (can be relatives)</li>
            <li>2. 2 Shareholders (can be same as directors)</li>
            <li>3. There is no minimum capital required</li>
          </ol>
          <br>
          We can assist you with company registration anywhere in India including Pune, Mumbai, Bangalore, Hyderabad and Chennai. Private Limited Company registration in Bangalore will be most suitable for a startup engaged in IT, artificial intelligence etc. Bangalore is known to be silicon valley of India. The best human resources for IT can be found in Bangalore.
          <br>
          <br>
          Private Limited Company registration in Mumbai is most suitable for startups engaged in Fintech. Most of the corporate offices of banks are based out of Mumbai.
        </div>
      </div>
    </div>
  </div>
</section>
<!-- ***** About Us Area End *****  -->
<!-- ***** Work Process Area Start *****  -->
<section class="work_process_area section_padding_90_70 clearfix">
  <div class="container">
    <div class="row">
      <!-- Heading Text -->
      <div class="col-12">
        <div class="section-heading work text-center">
          <i class="fa fa-wrench" aria-hidden="true"></i>
          <h2>Work process</h2>
          <span>What we do</span>
        </div>
      </div>
    </div>
    <div class="row">
      <!-- Single Content Text -->
      <div class="col-12 col-md-12">
        <div class="work_process_single_content">
          <!-- Icon -->
          <div class="work_process_icon">
            <i class="fa fa-rocket" aria-hidden="true"></i>
          </div>
          <h5>Step 1</h5>
          <p>
            Send the scanned copy of below mentioned documents to our mail ID :
            <a href="mailto:arthanitifincorp@gmail.com">
              arthanitifincorp@gmail.com
          </p>
        </div>
      </div>
      <!-- Single Content Text -->
      <div class="col-12 col-md-12">
      <div class="work_process_single_content">
      <!-- Icon -->
      <div class="work_process_icon">
      <i class="fa fa-rocket" aria-hidden="true"></i>
      </div>
      <h5>Step 2</h5>
      <p>We prepare MOA, AOA and other documents and submit the forms to ROC</p>
      </div>
      </div>
      <!-- Single Content Text -->
      <div class="col-12 col-md-12">
      <div class="work_process_single_content">
      <div class="work_process_icon">
      <i class="fa fa-bolt" aria-hidden="true"></i>
      </div>
      <h5>Step 3</h5>
      <p>The company gets incorporated in 10-12 working days.</p>
      </div>
      </div>
    </div>
  </div>
</section>
<section class="our_price_table_area section_padding_90_70">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="section-heading text-center">
          <i class="fa fa-gift" aria-hidden="true"></i>
          <h2>Best Pricing</h2>
          <span>Our Offers</span>
        </div>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-6 col-lg-4">
      </div>
      <div class="col-md-6 col-lg-4">
        <div class="pricing-plan featured wow fadeInUp" data-wow-delay="0.4s">
          <div class="pricing-head">
            <div class="name">
              <h4>Startup Power Pack</h4>
            </div>
            <div class="price">
              <h5><span></span>9999 + 1000</h5>
              <div class="duration">
                <p>One Time</p>
              </div>
            </div>
          </div>
          <div class="pricing-body">
            <ul>
              <li>2 Class 2 Digital Signatures</li>
              <li>2 Director Identification Numbers</li>
              <li>1 RUN Name Approval * (name approval charged extra depending on availability of name)</li>
              <li>Upto 5 Lakhs Authorized Capital</li>
              <li>Incorporation Fee</li>
              <li>Stamp Duty</li>
              <li>Incorporation Certificate</li>
              <li>Current Account Opening Letter</li>
              <li>PAN & TAN</li>
              <li>Commencement of Business Certificate</li>
              <li>Incorporation Kit</li>
              <li>Hard-copy Share Certificates</li>
              <li>GST Registration </li>
              <li>3 months GST Filling </li>
              <li>Free Consultancy with in person meeting</li>
            </ul>
          </div>
          <div class="pricing-footer mt-30">
            <a href="#" class="fancy-btn">Contact Us</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="our_price_table_area section_padding_90_70">
  <div class="container">
    <div class="row">
      <div class="jumbotron">
        <h1 class="display-4">FAQs!</h1>
        <div id="accordion">
          <div class="card">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                How do I start a Private Limited Company?
                </button>
              </h5>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
              <div class="card-body">
                Starting a company is easy through StartupNetworkIndia. All that is required are PAN card of the promoters, address proof and bank statement copies of the promoters along with address proof for the registered office address. A company can be started in about 10 - 15 days. If you have the necessary documents, sign up for one of our packages and have a company registered with guidance from one of our Advisors.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                What is Limited Liability Protection?
                </button>
              </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
              <div class="card-body">
                Limited liability is the status of being legally responsible only to a limited amount for debts of a company. Unlike proprietorships and partnerships, in a private limited company the liability of the shareholders in respect of the company’s liabilities is limited. In other words, the liability of the shareholders of a company is limited only to the value of shares taken up by them.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                What is authorized capital of private limited company?
                </button>
              </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="card-body">
                Authorised capital is the maximum value of equity shares that can be issued by a company. On the other hand, paid up capital is the amount of shares issued by the company to shareholders. Authorised capital can be increased after incorporation at anytime to issue additional shares to the shareholders.
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Heading Text -->
    </div>
  </div>
</section>
@endsection
