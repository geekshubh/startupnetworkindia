<!DOCTYPE html>
<html lang="en">
<meta name="csrf-token" content="{{ csrf_token() }}">
<head>
    @include('partials.head')
</head>
<body>
  @include('partials.header')
  @yield('content')
  @include('partials.contact-us')
  @include('partials.footer')
  @include('partials.javascripts')
</body>
</html>
