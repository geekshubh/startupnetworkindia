@extends('layouts.app')
@section('content')
<!-- ***** About Us Area Start *****  -->
<section class="about_us_area section_padding_90_100 clearfix" id="about">
  <div class="container">
    <div class="row">
      <!-- Heading Text -->
      <div class="col-12">
        <div class="section-heading text-center">
          <h2>One Person Company</h2>
        </div>
      </div>
    </div>
    <div class="row align-items-center">
      <div class="col-12 col-md-12">
        <!-- About us Content -->
        <div class="about_us_content">
          <h2>What is a One Person Company?</h2>
          The concept of One Person Company in India was introduced through the Companies Act, 2013 to support entrepreneurs who on their own are capable of starting a venture by allowing them to create a single person economic entity. One of the biggest advantages of a One Person Company (OPC) is that there can be only one member in a OPC, while a minimum of two members are required for incorporating and maintaining a Private Limited Company or a Limited Liability Partnership (LLP). Similar to a Company, a One Person Company is a separate legal entity from its promoter, offering limited liability protection to its sole shareholder, while having continuity of business and being easy to incorporate.
          <br>
          <br>
          Though a One Person Company allows a lone Entrepreneur to operate a corporate entity with limited liability protection, a OPC does have a few limitations. For instance, every One Person Company (OPC) must nominate a nominee Director in the MOA and AOA of the company - who will become the owner of the OPC in case the sole Director is disabled. Also, a One Person Company must be converted into a Private Limited Company if it crosses an annual turnover of Rs.2 crores and must file audited financial statements with the Ministry of Corporate Affairs at the end of each Financial Year like all types of Companies. Therefore, it is important for the Entrepreneur to carefully consider the features of a One Person Company prior to incorporation.
          <br>
          <br>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- ***** About Us Area End *****  -->
<!-- ***** Work Process Area Start *****  -->
<section class="work_process_area section_padding_90_70 clearfix">
  <div class="container">
    <div class="row">
      <!-- Heading Text -->
      <div class="col-12">
        <div class="section-heading work text-center">
          <i class="fa fa-wrench" aria-hidden="true"></i>
          <h2>Work process</h2>
          <span>What we do</span>
        </div>
      </div>
    </div>
    <div class="row">
      <!-- Single Content Text -->
      <div class="col-12 col-md-12">
        <div class="work_process_single_content">
          <!-- Icon -->
          <div class="work_process_icon">
            <i class="fa fa-rocket" aria-hidden="true"></i>
          </div>
          <h5>Step 1</h5>
          <p>
            Send the scanned copy of below mentioned documents to our mail ID :
            <a href="mailto:arthanitifincorp@gmail.com">
              arthanitifincorp@gmail.com
          </p>
        </div>
      </div>
      <!-- Single Content Text -->
      <div class="col-12 col-md-12">
      <div class="work_process_single_content">
      <!-- Icon -->
      <div class="work_process_icon">
      <i class="fa fa-rocket" aria-hidden="true"></i>
      </div>
      <h5>Step 2</h5>
      <p>We complete the necessary formalities.</p>
      </div>
      </div>
      <!-- Single Content Text -->
      <div class="col-12 col-md-12">
      <div class="work_process_single_content">
      <div class="work_process_icon">
      <i class="fa fa-bolt" aria-hidden="true"></i>
      </div>
      <h5>Step 3</h5>
      <p>The one person company gets incorporated in 15 working days.</p>
      </div>
      </div>
    </div>
  </div>
</section>
<section class="our_price_table_area section_padding_90_70">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="section-heading text-center">
          <i class="fa fa-gift" aria-hidden="true"></i>
          <h2>Best Pricing</h2>
          <span>Our Offers</span>
        </div>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-6 col-lg-4">
      </div>
      <div class="col-md-6 col-lg-4">
        <div class="pricing-plan featured wow fadeInUp" data-wow-delay="0.4s">
          <div class="pricing-head">
            <div class="name">
              <h4>Startup Power Pack</h4>
            </div>
            <div class="price">
              <h5><span></span>9999 + 1000</h5>
              <div class="duration">
                <p>One Time</p>
              </div>
            </div>
          </div>
          <div class="pricing-body">
            <ul>
              <li>2 Class 2 Digital Signatures</li>
              <li>1 Director Identification Numbers</li>
              <li>1 RUN Name Approval * (name approval charged extra depending on availability of name)</li>
              <li>Upto 10 Lakhs Authorized Capital</li>
              <li>Incorporation Fee</li>
              <li>Stamp Duty 4</li>
              <li>Current Account Opening Letter</li>
              <li>PAN & TAN</li>
              <li>Commencement of Business Certificate</li>
              <li>Incorporation Kit</li>
              <li>Hard-copy Share Certificates</li>
              <li>GST Registration </li>
              <li>3 months GST Filling </li>
              <li>Free Consultancy with in person meeting</li>
            </ul>
          </div>
          <div class="pricing-footer mt-30">
            <a href="#" class="fancy-btn">Contact Us</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="our_price_table_area section_padding_90_70">
  <div class="container">
    <div class="row">
      <div class="jumbotron">
        <h1 class="display-4">FAQs!</h1>
        <div id="accordion">
          <div class="card">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                How long is the incorporation of the Company valid for?
                </button>
              </h5>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
              <div class="card-body">
                Once a Company is incorporated, it will be active and in-existence as long as the annual compliances are met with regularly. In case, annual compliances are not complied with, the Company will become a Dormant Company and maybe struck off from the register after a period of time. A struck-off Company can be revived for a period of upto 20 years.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                How many people are required to incorporate a One Person Company?
                </button>
              </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
              <div class="card-body">
                To incorporate a One Person Company, a Director and a nominee is required. A nominee member is one, who shall, in the event of promoter member`s death or incapacitation become a member of the Company.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                What is the capital required to start a One Person Company?
                </button>
              </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="card-body">
                One Person Company can be started with any amount of capital. However, fee must be paid to the Government for issuing a minimum of shares worth Rs.1 lakh [Authorized Capital Fee] during the incorporation of the OPC. There is no requirement to show proof of capital invested during the incorporation process.
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Heading Text -->
    </div>
  </div>
</section>
@endsection
