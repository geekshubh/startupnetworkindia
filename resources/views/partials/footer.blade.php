<!-- ***** Bottom Footer Area Start *****  -->
<div class="bottom-footer-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="footer_area">
                    <!-- Footer Text -->
                    <div class="copywrite_text text-center">
                        <p>Made With <i class="fa fa-heart"></i> in INDIA</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ***** Bottom Footer Area End *****  -->
