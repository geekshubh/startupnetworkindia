<script src="{{ url('assets/js/jquery/jquery-2.2.4.min.js')}}"></script>
<!-- Popper js -->
<script src="{{ url('assets/js/bootstrap/popper.min.js')}}"></script>
<!-- Bootstrap-4 Beta JS -->
<script src="{{ url('assets/js/bootstrap/bootstrap.min.js')}}"></script>
<!-- All Plugins JS -->
<script src="{{ url('assets/js/plugins/plugins.js')}}"></script>
<script src="{{ url('assets/js/classy-nav.js')}}"></script>
<!-- Active JS -->
<script src="{{ url('assets/js/active.js')}}"></script>
<!-- Google Maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAwuyLRa1uKNtbgx6xAJVmWy-zADgegA2s"></script>
<script src="{{ url('assets/js/map-active.js')}}"></script>
