<header class="header-area">
    <div class="classy-nav-container breakpoint-off">
        <div class="container">
            <nav class="classy-navbar justify-content-between" id="classyNav">

                <a class="nav-brand" href="#"><img src ="{{ url('assets/img/snitransparent.png')}}"></a>

                <!-- Navbar Toggler -->
                <div class="classy-navbar-toggler">
                    <span class="navbarToggler"><span></span><span></span><span></span></span>
                </div>

                <!-- Menu -->
                <div class="classy-menu">

                    <!-- close btn -->
                    <div class="classycloseIcon">
                        <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>

                    <!-- Nav Start -->
                    <div class="classynav">
                        <ul id="nav">
                            <li><a href="#">Startups</a>
                                <ul class="dropdown">
                                    <li><a href="{{ route('company-registration-private-limited') }}">Private Limited Company</a></li>
                                    <li><a href="{{ route('llp') }}">Limited Liability Partnership</a></li>
                                    <li><a href="{{ route('proprietorship') }}">Proprietorship</a></li>
                                    <li><a href="{{ route('opc') }}">One Person Company</a></li>
                                    <li><a href="{{ route('partnership') }}">Partnership</a></li>
                                </ul>
                            </li>
                            <li><a href="#about">About</a></li>
                            <li><a href="#contact">Contact</a></li>
                        </ul>
                    </div>
                    <!-- Nav End -->
                </div>
            </nav>
        </div>
    </div>
</header>
