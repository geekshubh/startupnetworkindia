<section class="contact_us_area section_padding_90_0" id="contact">
  <div class="container">
    <div class="row">
      <!-- Heading Text -->
      <div class="col-12">
        <div class="section-heading text-center">
          <i class="fa fa-envelope" aria-hidden="true"></i>
          <h2>Get In Touch</h2>
          <span>Contact</span>
        </div>
      </div>
    </div>
    <div class="row align-items-center">
      <div class="col-12 col-md-9">
        <!-- Contact Form -->
        <div class="contact_from_area clearfix">
          <div class="contact_form">
            <form action="mail.php" method="post" id="main_contact_form">
              <div class="contact_input_area">
                <div id="success_fail_info"></div>
                <div class="row">
                  <div class="col-12">
                    <div class="conatct_heading">
                      <h2>Drop Line Here</h2>
                    </div>
                  </div>
                  <!-- Form Group -->
                  <div class="col-12 col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" name="name" id="name" placeholder="Your Name" required>
                    </div>
                  </div>
                  <!-- Form Group -->
                  <div class="col-12 col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" name="number" id="number" placeholder="Number *" required>
                    </div>
                  </div>
                  <!-- Form Group -->
                  <div class="col-12 col-md-6">
                    <div class="form-group">
                      <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" required>
                    </div>
                  </div>
                  <!-- Form Group -->
                  <div class="col-12 col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
                    </div>
                  </div>
                  <!-- Form Group -->
                  <div class="col-12">
                    <div class="form-group">
                      <textarea name="message" class="form-control" id="message" cols="30" rows="8" placeholder="Your Message *" required></textarea>
                    </div>
                  </div>
                  <!-- Button -->
                  <div class="col-12">
                    <button type="submit" class="fancy-btn">Send Now</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-3">
        <div class="contact_deatils">
          <!-- Content Info -->
          <div class="contact_info_area">
            <div class="contact_info">
              <i class="icofont icofont-social-google-map"></i>
              <h5>Location</h5>
              <p>4, Nakoda, Plot-93, Sector-12, Vashi, Navi Mumbai. 400703</p>
            </div>
            <!-- Content Info -->
            <div class="contact_info">
              <i class="icofont icofont-email"></i>
              <h5>E-mail</h5>
              <p>arthanitifincorp@gmail.com</p>
            </div>
            <!-- Content Info -->
            <div class="contact_info">
              <i class="icofont icofont-ui-dial-phone"></i>
              <h5>Phone</h5>
              <p>+91 77049 55555</p>
            </div>
            <!-- Icon -->
            <div class="footer_social_icon">
              <a href="#"><i class="fa fa-facebook" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Facebook"></i></a>
              <a href="#"> <i class="fa fa-twitter" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Twitter"></i></a>
              <a href="#"><i class="fa fa-google-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Google-Plus"></i></a>
              <a href="#"><i class="fa fa-instagram" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Instagram"></i></a>
              <a href="#"><i class="fa fa-linkedin" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Linkedin"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<br>
<br>
