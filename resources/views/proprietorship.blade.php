@extends('layouts.app')
@section('content')
<!-- ***** About Us Area Start *****  -->
<section class="about_us_area section_padding_90_100 clearfix" id="about">
  <div class="container">
    <div class="row">
      <!-- Heading Text -->
      <div class="col-12">
        <div class="section-heading text-center">
          <h2>Proprietorship</h2>
        </div>
      </div>
    </div>
    <div class="row align-items-center">
      <div class="col-12 col-md-12">
        <!-- About us Content -->
        <div class="about_us_content">
          <h2>What is Proprietorship?</h2>
          A sole proprietorship is a type of unregistered business entity that is owned, managed and controlled by one person. Sole proprietorship's are one of the most common forms of business in India, used by most micro and small businesses operating in the unorganised sectors. Proprietorships are very easy to start and have very minimal regulatory compliance requirement for started and operating. However, after the startup phase, proprietorship's do not offer the promoter a host of benefits such as limited liability proprietorship, corporate status, separate legal entity, independent existence, transferability, perpetual existence - which are desirable features for any business. Therefore, proprietorship registration is suited only for unorganised, small businesses that will remain small and/or have a limited period of existence.
          <br>
          <br>
          There is no mechanism provided by the Government of India for the registration of a Proprietorship. Therefore, the existence of a proprietorship must be established through tax registrations and other business registrations that a business is required to have as per the rules and regulations. For instance, VAT or Service Tax or GST Registration can be obtained in the name of the Proprietor to establish that the Proprietor is operating a business as a sole proprietorship. Thus, all the registrations for a proprietorship would be in the name of the Proprietor, making the Proprietor personally liable for all the liabilities of the Proprietorship.
          <br>
          <br>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- ***** About Us Area End *****  -->
<!-- ***** Work Process Area Start *****  -->
<section class="work_process_area section_padding_90_70 clearfix">
  <div class="container">
    <div class="row">
      <!-- Heading Text -->
      <div class="col-12">
        <div class="section-heading work text-center">
          <i class="fa fa-wrench" aria-hidden="true"></i>
          <h2>Work process</h2>
          <span>What we do</span>
        </div>
      </div>
    </div>
    <div class="row">
      <!-- Single Content Text -->
      <div class="col-12 col-md-12">
        <div class="work_process_single_content">
          <!-- Icon -->
          <div class="work_process_icon">
            <i class="fa fa-rocket" aria-hidden="true"></i>
          </div>
          <h5>Step 1</h5>
          <p>
            Send the scanned copy of below mentioned documents to our mail ID :
            <a href="mailto:arthanitifincorp@gmail.com">
              arthanitifincorp@gmail.com
          </p>
        </div>
      </div>
      <!-- Single Content Text -->
      <div class="col-12 col-md-12">
      <div class="work_process_single_content">
      <!-- Icon -->
      <div class="work_process_icon">
      <i class="fa fa-rocket" aria-hidden="true"></i>
      </div>
      <h5>Step 2</h5>
      <p>We complete the necessary formalties</p>
      </div>
      </div>
      <!-- Single Content Text -->
      <div class="col-12 col-md-12">
      <div class="work_process_single_content">
      <div class="work_process_icon">
      <i class="fa fa-bolt" aria-hidden="true"></i>
      </div>
      <h5>Step 3</h5>
      <p>The Proprietorship gets incorporated within 5-7 days.</p>
      </div>
      </div>
    </div>
  </div>
</section>
<section class="our_price_table_area section_padding_90_70">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="section-heading text-center">
          <i class="fa fa-gift" aria-hidden="true"></i>
          <h2>Best Pricing</h2>
          <span>Our Offers</span>
        </div>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-6 col-lg-4">
      </div>
      <div class="col-md-6 col-lg-4">
        <div class="pricing-plan featured wow fadeInUp" data-wow-delay="0.4s">
          <div class="pricing-head">
            <div class="name">
              <h4>Startup Power Pack</h4>
            </div>
            <div class="price">
              <h5><span></span>2999</h5>
              <div class="duration">
                <p>One Time</p>
              </div>
            </div>
          </div>
          <div class="pricing-body">
            <ul>
              <li>Udyog Aadhar Registration</li>
              <li>Bank Account Opening</li>
              <li>FSSAI * (3999 / year)</li>
              <li>IEC * (2000 one time)</li>
              <li>TRADEMARK * (7500 one time)</li>
              <li>GST Registration * (4000 one time)</li>
              <li>GST Filling * (Free for 3 months if GST Registration taken)</li>
              <li>Counsultancy will be provided free and in person</li>
            </ul>
          </div>
          <div class="pricing-footer mt-30">
            <a href="#" class="fancy-btn">Contact Us</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="our_price_table_area section_padding_90_70">
  <div class="container">
    <div class="row">
      <div class="jumbotron">
        <h1 class="display-4">FAQs!</h1>
        <div id="accordion">
          <div class="card">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                How many people are required to start a Proprietorship?
                </button>
              </h5>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
              <div class="card-body">
                Only one person is required to start a Proprietorship and a Proprietorship can have only one promoter.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                What are the requirements to be a Proprietor?
                </button>
              </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
              <div class="card-body">
                The Proprietor must be an Indian citizen and a Resident of India. Non-Resident Indians and Persons of Indian Origin can only invest in a Proprietorship with prior approval of the Government of India.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                What are the documents required to start a Proprietorship?
                </button>
              </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="card-body">
                PAN Card for the Proprietor along with identity and address proof is sufficient to start a Proprietorship and obtain other registration, as applicable or required.
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Heading Text -->
    </div>
  </div>
</section>
@endsection
