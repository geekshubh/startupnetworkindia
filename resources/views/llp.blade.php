@extends('layouts.app')
@section('content')
<!-- ***** About Us Area Start *****  -->
<section class="about_us_area section_padding_90_100 clearfix" id="about">
  <div class="container">
    <div class="row">
      <!-- Heading Text -->
      <div class="col-12">
        <div class="section-heading text-center">
          <h2>Limited Liability Partnership</h2>
        </div>
      </div>
    </div>
    <div class="row align-items-center">
      <div class="col-12 col-md-12">
        <!-- About us Content -->
        <div class="about_us_content">
          <h2>What is Limited Liability Partnership?</h2>
          Limited Liability Partnership (LLP) was introduced in India by way of the Limited Liability Partnership Act, 2008. The basic premise behind the introduction of Limited Liability Partnership (LLP) is to provide a form of business entity that is simple to maintain while providing limited liability to the owners. Since, its introduction in 2010, LLPs have been well received with over 1 lakhs registrations so far until September, 2014.
          <br>
          <br>
          The main advantage of a Limited Liability Partnership over a traditional partnership firm is that in a LLP, one partner is not responsible or liable for another partner's misconduct or negligence. A LLP also provides limited liability protection for the owners from the debts of the LLP. Therefore, all partners in a LLP enjoy a form of limited liability protection for each individual's protection within the partnership, similar to that of the shareholders of a private limited company. However, unlike private limited company shareholder, the partners of a LLP have the right to manage the business directly.
          <br>
          <br>
          LLP is one of the easiest form of business to incorporate and manage in India. With an easy incorporation process and simple compliance formalities, LLP is preferred by Professionals, Micro and Small businesses that are family owned or closely-held. Since, LLPs are not capable of issuing equity shares, LLP should be used for any business that has plans for raising equity funds during its lifecycle.
        </div>
      </div>
    </div>
  </div>
</section>
<!-- ***** About Us Area End *****  -->
<!-- ***** Work Process Area Start *****  -->
<section class="work_process_area section_padding_90_70 clearfix">
  <div class="container">
    <div class="row">
      <!-- Heading Text -->
      <div class="col-12">
        <div class="section-heading work text-center">
          <i class="fa fa-wrench" aria-hidden="true"></i>
          <h2>Work process</h2>
          <span>What we do</span>
        </div>
      </div>
    </div>
    <div class="row">
      <!-- Single Content Text -->
      <div class="col-12 col-md-12">
        <div class="work_process_single_content">
          <!-- Icon -->
          <div class="work_process_icon">
            <i class="fa fa-rocket" aria-hidden="true"></i>
          </div>
          <h5>Step 1</h5>
          <p>
            Send the scanned copy of below mentioned documents to our mail ID :
            <a href="mailto:arthanitifincorp@gmail.com">
              arthanitifincorp@gmail.com
          </p>
        </div>
      </div>
      <!-- Single Content Text -->
      <div class="col-12 col-md-12">
      <div class="work_process_single_content">
      <!-- Icon -->
      <div class="work_process_icon">
      <i class="fa fa-rocket" aria-hidden="true"></i>
      </div>
      <h5>Step 2</h5>
      <p>We complete the necessary formalities</p>
      </div>
      </div>
      <!-- Single Content Text -->
      <div class="col-12 col-md-12">
      <div class="work_process_single_content">
      <div class="work_process_icon">
      <i class="fa fa-bolt" aria-hidden="true"></i>
      </div>
      <h5>Step 3</h5>
      <p>The company gets incorporated in 10-12 working days.</p>
      </div>
      </div>
    </div>
  </div>
</section>
<section class="our_price_table_area section_padding_90_70">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="section-heading text-center">
          <i class="fa fa-gift" aria-hidden="true"></i>
          <h2>Best Pricing</h2>
          <span>Our Offers</span>
        </div>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-6 col-lg-4">
      </div>
      <div class="col-md-6 col-lg-4">
        <div class="pricing-plan featured wow fadeInUp" data-wow-delay="0.4s">
          <div class="pricing-head">
            <div class="name">
              <h4>Startup Power Pack</h4>
            </div>
            <div class="price">
              <h5><span></span>9999 + 1000</h5>
              <div class="duration">
                <p>One Time</p>
              </div>
            </div>
          </div>
          <div class="pricing-body">
            <ul>
              <li>2 Class 2 Digital Signature </li>
              <li>2 Designated Partner Identification Number (DPIN)</li>
              <li>LLP Name Approval * (name approval charged extra depending on availability of name)</li>
              <li>LLP Deed Drafting</li>
              <li>Incorporation Fees</li>
              <li>1 Lakh Capital</li>
              <li>Incorporation Certificate</li>
              <li>LLP Incorporation Kit</li>
              <li>PAN & TAN</li>
              <li>50+ Document Formats</li>
              <li>Current Account Opening Assistance</li>
              <li>GST Registration</li>
              <li>GST Filling for 3 months</li>
              <li>Free Consultancy with in person meeting</li>
            </ul>
          </div>
          <div class="pricing-footer mt-30">
            <a href="#" class="fancy-btn">Contact Us</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="our_price_table_area section_padding_90_70">
  <div class="container">
    <div class="row">
      <div class="jumbotron">
        <h1 class="display-4">FAQs!</h1>
        <div id="accordion">
          <div class="card">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                How many people are required to incorporate a LLP?
                </button>
              </h5>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
              <div class="card-body">
                To incorporate a Limited Liability Partnership, a minimum of two people are required. A Limited Liability Partnership must have a minimum of two Partners and can have a maximum of any number of Partners.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                What are the requirements to be a Partner in a LLP?
                </button>
              </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
              <div class="card-body">
                The Designated Partners needs to be over 18 years of age and must be a natural person. There are no limitations in terms of citizenship or residency. Therefore, the LLP Act 2008 allows Foreign Nationals including Foreign Companies & LLPs to incorporate a LLP in India provided at least one designated partner is resident of India.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                What is the capital required to start a Limited Liability Partnership?
                </button>
              </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="card-body">
                You can start a Limited Liability Partnership with any amount of capital. There is no requirement to show proof of capital invested during the incorporation process. Partner's contribution may consist of both tangible and/or intangible property and any other benefit to the LLP.
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Heading Text -->
    </div>
  </div>
</section>
@endsection
