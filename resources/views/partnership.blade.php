@extends('layouts.app')
@section('content')
<!-- ***** About Us Area Start *****  -->
<section class="about_us_area section_padding_90_100 clearfix" id="about">
  <div class="container">
    <div class="row">
      <!-- Heading Text -->
      <div class="col-12">
        <div class="section-heading text-center">
          <h2>Partnernship Firm</h2>
        </div>
      </div>
    </div>
    <div class="row align-items-center">
      <div class="col-12 col-md-12">
        <!-- About us Content -->
        <div class="about_us_content">
          <h2>What is a Partnernship Firm?</h2>
          A Partnership Firm is a popular form of business constitution for businesses that are owned, managed and controlled by an Association of People for profit. Partnership firms are relatively easy to start are is prevalent amongst small and medium sized businesses in the unorganized sectors. With the introduction of Limited Liability Partnerships in India, Partnership Firms are fast losing their prevalence due to the added advantages offered by a Limited Liability Partnership.
          <br>
          <br>
          There are two types of Partnership firms, registered and un-registered Partnership firm. It is not compulsory to register a Partnership firm; however, it is advisable to register a Partnership firm due to the added advantages. Partnership firms are created by drafting a Partnership deed amongst the Partners and IndiaFilings can help start a registered or un-registered Partnership firm in India.
          <br>
          <br>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- ***** About Us Area End *****  -->
<!-- ***** Work Process Area Start *****  -->
<section class="work_process_area section_padding_90_70 clearfix">
  <div class="container">
    <div class="row">
      <!-- Heading Text -->
      <div class="col-12">
        <div class="section-heading work text-center">
          <i class="fa fa-wrench" aria-hidden="true"></i>
          <h2>Work process</h2>
          <span>What we do</span>
        </div>
      </div>
    </div>
    <div class="row">
      <!-- Single Content Text -->
      <div class="col-12 col-md-12">
        <div class="work_process_single_content">
          <!-- Icon -->
          <div class="work_process_icon">
            <i class="fa fa-rocket" aria-hidden="true"></i>
          </div>
          <h5>Step 1</h5>
          <p>
            Send the scanned copy of below mentioned documents to our mail ID :
            <a href="mailto:arthanitifincorp@gmail.com">
              arthanitifincorp@gmail.com
          </p>
        </div>
      </div>
      <!-- Single Content Text -->
      <div class="col-12 col-md-12">
      <div class="work_process_single_content">
      <!-- Icon -->
      <div class="work_process_icon">
      <i class="fa fa-rocket" aria-hidden="true"></i>
      </div>
      <h5>Step 2</h5>
      <p>We complete the necessary formalties</p>
      </div>
      </div>
      <!-- Single Content Text -->
      <div class="col-12 col-md-12">
      <div class="work_process_single_content">
      <div class="work_process_icon">
      <i class="fa fa-bolt" aria-hidden="true"></i>
      </div>
      <h5>Step 3</h5>
      <p>The Partnership firm gets incorporated within 9-12 days.</p>
      </div>
      </div>
    </div>
  </div>
</section>
<section class="our_price_table_area section_padding_90_70">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="section-heading text-center">
          <i class="fa fa-gift" aria-hidden="true"></i>
          <h2>Best Pricing</h2>
          <span>Our Offers</span>
        </div>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-6 col-lg-4">
      </div>
      <div class="col-md-6 col-lg-4">
        <div class="pricing-plan featured wow fadeInUp" data-wow-delay="0.4s">
          <div class="pricing-head">
            <div class="name">
              <h4>Startup Power Pack</h4>
            </div>
            <div class="price">
              <h5><span></span>9999</h5>
              <div class="duration">
                <p>One Time</p>
              </div>
            </div>
          </div>
          <div class="pricing-body">
            <ul>
              <li>Basic partnership deed drafting by a Lawyer</li>
              <li>Registrar of Firms, GoM Registration</li>
              <li>PAN & TAN</li>
              <li>Current Account Opening Assistance</li>
              <li>GST Registration</li>
              <li>GST Filling (Free for 3 months)</li>
              <li>Counsultancy will be provided free and in person</li>
            </ul>
          </div>
          <div class="pricing-footer mt-30">
            <a href="#" class="fancy-btn">Contact Us</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="our_price_table_area section_padding_90_70">
  <div class="container">
    <div class="row">
      <div class="jumbotron">
        <h1 class="display-4">FAQs!</h1>
        <div id="accordion">
          <div class="card">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                How many people are required to start a Partnership firm?
                </button>
              </h5>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
              <div class="card-body">
                A minimum of two Persons is required to start a Partnership firm. A maximum number of 20 Partners are allowed in a Partnership firm.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                What are the requirements to be a Partner in a Partnership firm?
                </button>
              </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
              <div class="card-body">
                The Partner must be an Indian citizen and a Resident of India. Non-Resident Indians and Persons of Indian Origin can only invest in a Proprietorship with prior approval of the Government of India.
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThree">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                What are the documents required to start a Partnership firm?
                </button>
              </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="card-body">
                PAN Card for the Partners along with identity and address proof is required. It is recommended to draft a Partnership deed and have it signed by all the Partners in the firm.
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Heading Text -->
    </div>
  </div>
</section>
@endsection
