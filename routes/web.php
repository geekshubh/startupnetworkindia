<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('company-registration-private-limited',['uses'=>'PagesController@pvt_ltd','as'=>'company-registration-private-limited']);
Route::get('proprietorship',['uses'=>'PagesController@proprietorship','as'=>'proprietorship']);
Route::get('llp',['uses'=>'PagesController@llp','as'=>'llp']);
Route::get('partnership',['uses'=>'PagesController@partnership','as'=>'partnership']);
Route::get('one-person-company',['uses'=>'PagesController@opc','as'=>'opc']);
